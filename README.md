# Hello World

Example from https://develop.kde.org/docs/kirigami/introduction-getting_started/

## Changes

This import in `main.qml`

```
import org.kde.kirigami 2.5 as Kirigami
```

Using the original `2.31` version I had this runtime error

```
./build/bin/helloworld
QQmlApplicationEngine failed to load component
qrc:/main.qml:4:1: module "org.kde.kirigami" version 2.31 is not installed
```

So I used `dpkg -l | grep kirigami` to try to find out what version I had installed here (Kubuntu 21.10).

## Build

```
# build requirements
sudo apt install build-essential extra-cmake-modules cmake qtbase5-dev qtdeclarative5-dev libqt5svg5-dev qtquickcontrols2-5-dev qml-module-org-kde-kirigami2 kirigami2-dev libkf5i18n-dev gettext libkf5coreaddons-dev qml-module-qtquick-layouts

cmake -B build . && cmake --build build
```

## Run

```
./build/bin/helloworld
```
